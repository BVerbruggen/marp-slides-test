---
marp: true
#paginate: true
title: Network Programmability & Python on the ISR4k Routers 
theme: default
class: invert
style: |
    a,h1 {
        color: hotpink
    }
---
<!-- _class: _invert -->
# Network Programmability & Python on the ISR4k Routers 

Bram Verbruggen - Thomas More University - Belgium
bram.verbruggen@thomasmore.be

---
<!-- paginate: true -->
### **My background**
- ## 3 years of teaching 
- ## Cisco (CCNA, DevNet) and cloud related courses
- ## 12 years of consultancy & engineering work, mostly in IT security
- ## Applied Computer Science degree

---

### **My coding experience (after graduating)**

- ## Some Linux bash scripts
- ## Maybe some Perl? 
- ...

![bg grayscale blur:20px](img/2022-08-24-19-32-40.png)

---

### **My students**

- ## Cloud and Cybersecurity track
- ## 3rd phase, completed CCNA 1-3
- ## Have basic programming skills (Python)
- ## Some of them like to code...
- ## Most of them like **hands-on experience**

---

### **The mission**

## Enthuse these students about network and cloud automation 

![bg left](img/2022-08-24-19-23-58.png)

---

### **Enter... DevNet**

- ## Cisco Netacad course that covers these topics
- ## Requires **basic** Python knowledge
- ## Covers the basics: APIs, CI/CD and DevOps, automation tools like ansible
- ## Applicable to Cisco solutions, but also to a lot of cloud platforms like AWS, Azure, ...

---
<!-- _class: _invert -->

### **Python example**

## Getting routes from a Cisco router via RESTConf


```python
import json
import requests

#Disable SSL certificate warnings
requests.packages.urllib3.disable_warnings()



api_url = "https://192.168.10.1/restconf/data/ietf-routing:routing/routing-instance=default/routing-protocols/routing-protocol=static,1"

#set content types to yang in json format
headers = { "Accept" : "application/yang-data+json",
            "Content-type" : "application/yang-data+json"
            }

basicauth = ("cisco","cisco123!")

resp = requests.get(api_url,auth=basicauth,headers=headers,verify=False)

print(resp)
print(json.dumps(resp.json(),indent=4))
```

---
### **Back to school!**

# [SkillsForAll Python Essentials 1 (30 hours)](https://skillsforall.com/course/python-essentials-1)


---
<!-- _class: _invert -->
### **Back to school!**

# <!-- fit -->[Netacad PCAP: Programming Essentials in Python (75 hours)](https://www.netacad.com/courses/programming/pcap-programming-essentials-python)

---

### **Back to school!**


# [Practicepython.org exercises](https://www.practicepython.org/)

---


### **Back to school!**
![bg left contain](img/2022-08-26-00-55-42.png)
# [DevNet Associate + Accreditation](https://www.netacad.com/courses/infrastructure-automation/devnet-associate)

---

![bg opacity:20%](img/2022-08-25-16-28-12.png)
# Good times!! :partying_face:

---

<!-- _class: _invert -->
![bg right contain](img/2022-08-25-17-10-19.png)
# But...

---

![bg opacity:20%](img/2022-08-25-17-06-19.png)
### **Additions**

- ## Integration project using a cloud platform
- ## **Lab to implement network programmability on "real" ISR routers**


---

![bg contain](img/2022-08-25-17-33-28.png)

---

![bg contain](img/2022-08-25-17-34-30.png)

---
<!-- _class: _invert -->
### **Goal: write these functions**
```python
#Set interfaces on R1
put_interface(R1_mgmt_ip,"GigabitEthernet0/0/0","10.10.10.1","255.255.255.0")
put_interface(R1_mgmt_ip,"GigabitEthernet0/0/1","172.16.1.1","255.255.255.252")

#Set interfaces on R2
put_interface(R2_mgmt_ip,"GigabitEthernet0/0/0","10.10.20.1","255.255.255.0")
put_interface(R2_mgmt_ip,"GigabitEthernet0/0/1","172.16.1.2","255.255.255.252")
put_loopback(R2_mgmt_ip,"Loopback1","8.8.8.8","255.255.255.0")

#Set routes on R1
put_route_nexthop_ip(R1_mgmt_ip,"10.10.20.0/24","172.16.1.2")
put_route_nexthop_ip(R1_mgmt_ip,"0.0.0.0/0","172.16.1.2")

#Set routes on R2
put_route_nexthop_ip(R2_mgmt_ip,"10.10.10.0/24","172.16.1.1")
```
---

### **Goal: Test Automatic Problem Mitigation**

- ## Run script
- ## Test network
- ## Break it
- ## Re-run script to fix problem
- ## Test network

---

# Lessons learned - Best practices


---

# :-1: COVID impact :mask:

---

# <!-- fit -->:-1: [IOS Self-Signed Certificate Expiration on Jan. 1, 2020](https://www.cisco.com/c/en/us/support/docs/security-vpn/public-key-infrastructure-pki/215118-ios-self-signed-certificate-expiration-o.html):boom:

---

<!-- _class: _invert -->
# :+1: Students received the lab in advance :clipboard:

---

<!-- _class: _invert -->
# :+1: Motivating for students :sunglasses:

---
# Thanks!

Questions?
bram.verbruggen@thomasmore.be